<!DOCTYPE html>
<html lang="ua">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#f0f0f0">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#f0f0f0">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#f0f0f0">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <header class="page-header-wrap" role="banner">
        <div class="container">
            <div class="page-header">
                <a href="<?php echo site_url(); ?>" class="logo">
                  <img src="<?php echo faraday_get_option('header_logo'); ?>" alt="logo">
                </a>
                <div class="contact-number">
                    <p>
                        <img src="<?php echo faraday_get_option('header_phone_1_img'); ?>" alt="image">
                        <?php echo faraday_get_option('header_phone_1'); ?>
                    </p>
                    <p>
                        <img src="<?php echo faraday_get_option('header_phone_2_img'); ?>" alt="image">
                        <?php echo faraday_get_option('header_phone_2'); ?>
                    </p>
                </div>
                <div class="hamburger">
                    <span><img src="<?php echo get_template_directory_uri(); ?>/img/ic-hamburger-32-x-32-white.svg" alt="image"></span>
                </div>
                <!-- /.gamburger -->

                <nav class="navigation">
                    <a href="<?php echo site_url(); ?>" class="logo">
                      <img src="<?php echo faraday_get_option('header_logo_menu'); ?>" alt="logo">
                    </a>

                    <!--<span class="close">X</span>-->

                    <span class="close" aria-label="Закрыть"><span></span></span>

                    <?php wp_nav_menu( array(
                    'theme_location'  => 'header',
                    'fallback_cb'     => '__return_empty_string',
                    'container'       => '', 
                    'menu_class'      => '', 
                    'menu_id'         => '', 
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                  ) );  ?>

                </nav>

            </div>
        </div>
    </header>