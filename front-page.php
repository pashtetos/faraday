
<?php get_header(); ?>

	<?php get_template_part('template-parts/main-info'); ?>

	<?php get_template_part('template-parts/equipment-section'); ?>

	<?php get_template_part('template-parts/advantages-section'); ?>

	<?php get_template_part('template-parts/info-company'); ?>

	<?php get_template_part('template-parts/friends-company'); ?>

<?php get_footer(); ?>