<?php

/** 
 * Popup product order
 */
if ( isset($_POST['name'])  && 
     isset($_POST['tel'])   && 
     isset($_POST['product_link']) && 
    !empty($_POST['name'])  && 
    !empty($_POST['tel'])
) {
    $subject = 'Заявка на приобретение товара.';
    $to      = get_option('admin_email');
    $name    = $_POST['name'];
    $phone   = $_POST['tel'];
    $product_link = $_POST['product_link'];

    $message  = '<p><b>Тема: </b>Заявка с сайта  '.get_bloginfo('name').'</p>';
    $message .= '<p><b>Ссылка на страницу Товара: </b>'.$product_link.'</p>';
    $message .= '<p><b>Имя: </b>'.$name.'</p>';
    $message .= '<p><b>Телефон: </b>'.$phone.'</p>';

    if( isset($_POST['email']) && !empty($_POST['email']) ){
        $message .= '<p><b>E-mail: </b>'.$_POST['email'].'</p>';
    }

    if( isset($_POST['np']) && !empty($_POST['np']) ){
        $message .= '<p><b>№ відділення «Нової Пошти»: </b>'.$_POST['np'].'</p>';
    }

    $headers = 'From:'.get_bloginfo('name').'<wordpress@faraday.romchyk16.xyz>' . "\r\n";

    add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));

    wp_mail($to, $subject, $message, $headers);

    wp_redirect( $product_link.'/?order-send=success' ); 
    exit;
}