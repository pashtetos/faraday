<?php 

/**
* Registers a new post type "Project"
*/
function faraday_register_post_type_project() {

	$labels = array(
		'name'                => __( 'Проекты', 'faraday' ),
		'singular_name'       => __( 'Проект', 'faraday' ),
		'add_new'             => _x( 'Добавить Новый Проект', 'faraday', 'faraday' ),
		'add_new_item'        => __( 'Добавить Новый Проект', 'faraday' ),
		'edit_item'           => __( 'Изменить Проект', 'faraday' ),
		'new_item'            => __( 'Новый Проект', 'faraday' ),
		'view_item'           => __( 'Показать Проект', 'faraday' ),
		'search_items'        => __( 'Искать Проекты', 'faraday' ),
		'not_found'           => __( 'Проекты не найдены', 'faraday' ),
		'not_found_in_trash'  => __( 'Проекты не найдены в Корзине', 'faraday' ),
		'parent_item_colon'   => __( 'Родительский Проект:', 'faraday' ),
		'menu_name'           => __( 'Проекти', 'faraday' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array('faraday_category'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-clipboard',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'author', 'editor', 'custom-fields', 'page-attributes', 'post-formats', 'thumbnail'
			)
	);

	register_post_type( 'projects', $args );
}

add_action( 'init', 'faraday_register_post_type_project' );



/**
 * Create a taxonomy
 */
function faraday_service_taxonomies() {

	$labels = array(
		'name'					=> _x( 'Категории', 'Taxonomy Категории', 'faraday' ),
		'singular_name'			=> _x( 'Категория', 'Taxonomy Категория', 'faraday' ),
		'search_items'			=> __( 'Искать Категории', 'faraday' ),
		'popular_items'			=> __( 'Популярные Категории', 'faraday' ),
		'all_items'				=> __( 'Все Категории', 'faraday' ),
		'parent_item'			=> __( 'Родительская Категория', 'faraday' ),
		'parent_item_colon'		=> __( 'Родительская Категория', 'faraday' ),
		'edit_item'				=> __( 'Изменить Категорию', 'faraday' ),
		'update_item'			=> __( 'Обновить Категорию', 'faraday' ),
		'add_new_item'			=> __( 'Добавить Новую Категорию', 'faraday' ),
		'new_item_name'			=> __( 'Новое Имя Категории', 'faraday' ),
		'add_or_remove_items'	=> __( 'Добавить или удалить Категорию', 'faraday' ),
		'choose_from_most_used'	=> __( 'Choose from most used faraday', 'faraday' ),
		'menu_name'				=> __( 'Категории', 'faraday' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'projects_category', array( 'projects' ), $args );
}

add_action( 'init', 'faraday_service_taxonomies' );
