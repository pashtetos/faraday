<?php 
$phone_tel_1 = str_replace( " ", "", faraday_get_option('header_phone_1') );
$phone_tel_1 = str_replace( array(')', '('), array('',''), $phone_tel_1 ); 
$phone_tel_1 = str_replace( array('–', '–'), array('',''), $phone_tel_1 );

$phone_tel_2 = str_replace( " ", "", faraday_get_option('header_phone_2') );
$phone_tel_2 = str_replace( array(')', '('), array('',''), $phone_tel_2 ); 
$phone_tel_2 = str_replace( array('–', '–'), array('',''), $phone_tel_2 );  

?> 
  <footer class="page-footer" role="contentinfo">
      <div class="container">

          <div class="link-company">
              <a href="<?php echo faraday_get_option('footer_link_link'); ?>" class="link-site">
                <?php echo faraday_get_option('footer_link_title'); ?>
              </a>
              <a href="<?php echo faraday_get_option('footer_soc_link'); ?>" class="facebook icon-fb">
                <span><?php echo faraday_get_option('footer_soc_title'); ?></span>
              </a>
              <div class="numbers-phone">
                  <a href="tel:<?php echo $phone_tel_1; ?>" class="number-phone icon-phone">
                    <?php echo faraday_get_option('header_phone_1'); ?>
                    <span>,</span>
                  </a>
                  <a href="tel:<?php echo $phone_tel_2; ?>" class="number-phone"> 
                    <?php echo faraday_get_option('header_phone_2'); ?>
                  </a>
              </div>
              <!-- /.numbers-phone -->
              <a href="mailto:<?php echo faraday_get_option('header_email'); ?>" class="envelop icon-mail">
                <span><?php echo faraday_get_option('header_email'); ?></span>
              </a> 
          </div>
          <!-- /.company-social -->
      </div>
  </footer>
  <div class="overlay"></div>

  <!-- /.overlay -->

  <?php wp_footer(); ?>

  </body>
</html>