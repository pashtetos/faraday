<?php
/**
 * Template Name: О нас
 */
?>

<?php get_header(); ?>

    <?php while ( have_posts() ): the_post(); ?>

        <div class="about-us">
            <div class="container">
                <div class="about-us-block">
                    <div class="about-us-main">

                        <h1><?php the_title(); ?></h1>

                        <?php the_content(); ?>

                        <div class="about-us-info">
                            <?php echo get_post_meta( get_the_ID(), 'about_us_desc', true ); ?>
                        </div>
                        <!-- /.about-us-info -->

                    </div>
                    <!-- /.about-us-main -->

                    <div class="about-us-team">
                        <h2><?php echo get_post_meta( get_the_ID(), 'about_us_comand_title', true ); ?></h2>

                        <?php $items = get_post_meta( get_the_ID(), 'about_us_repeat', true ); ?>

                        <?php foreach( (array)$items as $item ): ?>

                            <div class="team-item">
                                <div class="team-item-img">
                                    <img src="<?php echo $item['about_us_image']; ?>" alt="image">
                                </div>
                                <!-- /.team-item-img -->
                                <div class="team-item-name"><?php echo $item['about_us_name']; ?></div>
                                <!-- /.team-item-name -->
                                <a href="<?php echo $item['about_us_soc_link']; ?>" class="team-item-profile">
                                    <?php echo $item['about_us_soc_name']; ?>
                                </a>
                                <!-- /.team-item-profile -->
                                <p>
                                    <?php echo $item['about_us_item_desc']; ?>
                                </p>
                                <a href="<?php echo $item['about_us_link']; ?>" class="detail-link">
                                    <?php echo $item['about_us_link_name']; ?>
                                </a>
                            </div>
                            <!-- /.about-us-team-item -->

                        <?php endforeach; ?>

                    </div>
                    <!-- /.about-us-team -->
                </div>
                <!-- /.about-us-block -->
            </div>
            <!-- /.container -->
        </div>

    <?php endwhile; ?>

<?php get_footer(); ?>