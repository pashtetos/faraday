<?php
/**
 * Template Name: Проекты
 */
?>

<?php get_header(); ?>

    <?php while ( have_posts() ): the_post(); ?>

        <div class="project-section">
            <div class="container">

                <h1><?php the_title(); ?></h1>

                <?php the_content(); ?>

                <div class="project-item-wrap">

                <?php $args = array( 
                  // 'posts_per_page' => 4, 
                  'post_type'      => 'projects', 
                  'post_status'    => 'publish' 
                ); 
                $query = new WP_Query( $args ); ?>

                <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                    <a href="<?php the_permalink(); ?>" class="project-item">
                        <div class="project-item-img">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                            <div class="project-item-title">
                                <h2><?php the_title(); ?></h2>
                                <span><?php echo get_post_meta( get_the_ID(), 'single_project_comand_subtitle', true ); ?></span>
                            </div>
                            <!-- /.project-item-title -->

                        </div>
                        <p><?php echo get_post_meta( get_the_ID(), 'single_project_comand_excerpt', true ); ?></p>
                        <!-- /.project-item-img -->
                        <span class="detail-link">Детальніше</span>
                    </a>
                    <!-- /.project-item -->

                <?php endwhile; endif; ?>

                </div>
                <!-- /.project-item-wrap -->

            </div>
            <!-- /.container -->
        </div>

    <?php endwhile; ?>

<?php get_footer(); ?>