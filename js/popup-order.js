jQuery(document).ready(function () {

    jQuery(".close").click(function () {
        jQuery(this).parent().addClass("closed");
    })

    jQuery("#submit_order").click(function () {

        if (jQuery('#order_product input[name="name"]').val() == "" && jQuery('#order_product input[name="tel"]').val() == "") {
            console.log('error');
        } else {
            $("#order_product").submit();
        }
    });


    jQuery('.styled-checkbox label').click(function () {

        var ID = jQuery(this).attr('for');

        if (jQuery('#' + ID).is(":checked")) {
            jQuery('#place_delivery').hide();
        } else {
            jQuery('#place_delivery').show();
        }
    });


    $('.notification.success').on('click', function () {
        $(this).fadeOut(400);
    });

    //
    // $(".tooltip-btn").hover(function() {
    //
    //     var target = $(this).attr("data-remodal-target");
    //
    //     $(this).remodal();
    // })


    $('.image-link').magnificPopup(
        {
            type: 'image',
            gallery: {enabled: true}
        }
    );

    $('#to-products').on('click', function(e){
        event.preventDefault();
        var id  = $(this).attr('href'),
            bottom = $('.search-result-section').offset().top;
        $('body,html').animate({scrollTop: bottom}, 1000);
    })

    $(".tooltip-btn").hover(function(){
        $(this).click();
    }).click(function(){
        //common function
    });
}); 