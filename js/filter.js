jQuery(document).ready(function () {

	var currentUrl = window.location;
	var url 	   = new URL(currentUrl);

	if(currentUrl.search){ 
		var paramCat = url.searchParams.get("cat"); 
	} else {
		var paramCat = 0;
	}

	if( paramCat.length > 0 ){
		var param = paramCat.split(',');

		if( param.length > 0 ){
			var i;
			for (i = 0; i < param.length; ++i) {
			    var check_id = param[i];
				jQuery('#'+check_id).attr('checked', true);
			}
		}
	}
		
	function insertParam(key, value){

	    key = encodeURI(key); 
	    value = encodeURI(value);
	    var kvp = document.location.search.substr(1).split('&');
	    var i=kvp.length; 
	    var x; 
	    while(i--) {
	        x = kvp[i].split('=');

	        if (x[0]==key){
	            x[1] = value;
	            kvp[i] = x.join('=');
	            break;
	        }
	    }

	    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

	    document.location.search = kvp.join('&'); 
	}

	jQuery('label').click(function() {

    	var catID   = jQuery(this).attr('for');
    	var currUrl = window.location.href;
		var url     = new URL(currUrl);
		var param   = url.searchParams.get("cat");

		if ( jQuery('#'+catID).is(":checked") ) {
			console.log('no checked');
			var param = paramCat.split(',');
			var newParam = jQuery.grep(param, function(value) {
			  return value != catID;
			});
			insertParam( 'cat', newParam.toString() );

		} else {
			console.log('checked');
			if ( param != 0 && param != null ) {
				var newParam = param+','+catID;
				insertParam('cat', newParam);
			} else {
				insertParam('cat', catID+',');
			}
		}
		   
	});

});
