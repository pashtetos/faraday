<?php
/**
 * Template Name: Категории Оборудования
 */
?>

<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<div class="category-equipment">
		  <div class="container">
		    <h1><?php echo get_the_title(); ?></h1>
		  </div>
		  <!-- /.container -->
		</div>
		<!-- /.category-equipment -->

		<?php get_template_part('template-parts/equipment-section'); ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?> 