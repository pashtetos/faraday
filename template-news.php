<?php
/**
 * Template Name: Новости
 */
?>

<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<div class="news-section">
		    <div class="container">
		        <div class="news-wrap">

		            <div class="news-filter">
		                <span class="close" aria-label="Закрыть"><span></span></span>
		                <p>Категорії:</p>

		                <?php $categories = get_categories( array(
		                	'hide_empty' => false,
		                	'exclude'    => 1,
							'orderby' 	 => 'name',
							'order'   	 => 'ASC'
						)); ?>

						<ul>
							<?php foreach( $categories as $category ): ?>
								<li>
			                        <div class="styled-checkbox">
			                            <input type="checkbox" id="<?php echo $category->term_id; ?>">
			                            <label for="<?php echo $category->term_id; ?>">
			                                <?php echo $category->name; ?>
			                            </label>
			                        </div>
			                    </li>
							<?php endforeach; ?>
		                </ul>
		            </div>

		            <div class="news-block">
		                <h1>
		                    <?php the_title(); ?>
		                    <button class="btn btn-secondary toggle-news-filter icon-filter">Категорії</button>
		                </h1>

		                <?php  
		                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		                if (isset($_GET['cat'])) {
		                	$args  = array(
			                	'paged' 		 => $paged,
								// 'posts_per_page' => 2,
								'posts_status' 	 => 'publish',
								'cat'  			 => explode( ',', $_GET['cat'] ),
								'post_type' 	 => 'post',
							);
		                } else {
		                	$args  = array(
			                	'paged' 		 => $paged,
								// 'posts_per_page' => 2,
								'posts_status' 	 => 'publish',
								'post_type' 	 => 'post',
							);
		                }

						$query = new WP_Query( $args ); ?>

						<?php if ( $query->have_posts() ): ?>
							<?php while ( $query->have_posts() ): $query->the_post(); ?>

								<a href="<?php the_permalink(); ?>" class="news-item">
				                    <div class="news-item-img">
				                        <img src="<?php the_post_thumbnail_url(); ?>" alt="image">
				                    </div>
				                    <!-- /.news-item-img -->
				                    <span class="news-item-category">
				                        EV-net
				                    </span>
				                    <!-- /.news-item-category -->
				                    <h2><?php the_title(); ?></h2>
				                    <p>
				                        <?php the_excerpt(); ?>
				                    </p>
				                    <span class="news-item-date">
				                        <?php the_date(); ?>
				                    </span>
				                    <!-- /.news-item-date -->
				                </a>
				                <!-- /.news-item -->

							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php else: ?>
							<h2>Новостей не найдено.</h2>
						<?php endif; ?>

						<?php faraday_pagination($query->max_num_pages); ?>

		            </div>

		        </div>
		        <!-- /.news-wrap -->

		    </div>
		    <!-- /.container -->
		</div>
		<!-- /.news-section -->

	<?php endwhile; endif; ?>

<?php get_footer(); ?>