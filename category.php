<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>

        <div class="news-section">
            <div class="container">
                <div class="news-wrap">
                    <div class="news-filter">
                        <span class="close" aria-label="Закрыть"><span></span></span>
                        <p>Категорії:</p>
                        <ul>
                            <li>
                                <div class="styled-checkbox">
                                    <input type="checkbox" id="1">
                                    <label for="1">
                                        EV-net
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="styled-checkbox">
                                    <input type="checkbox" id="2">
                                    <label for="2">
                                        EcoTown
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="styled-checkbox">
                                    <input type="checkbox" id="3">
                                    <label for="3">
                                        Пробіги
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="styled-checkbox">
                                    <input type="checkbox" id="4">
                                    <label for="4">
                                        Обладнання
                                    </label>
                                </div>
                                <!-- /.styled-checkbox -->

                            </li>
                        </ul>
                    </div>

                    <div class="news-block">
                        <h1>
                            Новини
                            <button class="btn btn-secondary toggle-news-filter icon-filter">Категорії</button>
                        </h1>

                        <?php while ( have_posts() ) : the_post(); ?>

                            <a href="<?php the_permalink() ?>" class="news-item">
                                <div class="news-item-img">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                                </div>
                                <!-- /.news-item-img -->

                                <span class="news-item-category">
                                    <?php $categories = get_the_category( $post->ID ); 

                                    foreach( (array)$categories as $category ){
                                        echo $category->name .',  ';
                                    } ?>
                                </span>
                                <!-- /.news-item-category -->

                                <h2><?php the_title(); ?></h2>
                                <p>
                                    <?php echo get_the_excerpt(); ?>
                                </p>
                                <span class="news-item-date">
                                    <?php the_date(); ?>
                                </span>
                                <!-- /.news-item-date -->
                            </a>
                            <!-- /.news-item -->

                        <?php endwhile; ?>

                        <div class="pagination">
                            <a href="" class="pagination-item">&larr;</a>
                            <a href="" class="pagination-item active">1</a>
                            <a href="" class="pagination-item">2</a>
                            <a href="" class="pagination-item">...</a>
                            <a href="" class="pagination-item">11</a>
                            <a href="" class="pagination-item">&rarr;</a>
                        </div>

                    </div>

                </div>
                <!-- /.news-wrap -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /.news-section -->

    <?php endif; ?>

<?php get_footer(); ?>