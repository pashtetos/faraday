<?php 

/**
 * Include theme scripts
 */
function faraday_scripts() {
	//==================================================== CSS ====================================================//
    wp_enqueue_style( 'magnific-popup-css', get_template_directory_uri() . '/css/magnific-popup.css' );
    wp_enqueue_style( 'style-min-css', get_template_directory_uri() . '/css/style.min.css' );
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/style.css' );

	//==================================================== JS ====================================================//
    wp_enqueue_script( 'script-min-js', get_template_directory_uri() . '/js/script.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/js/swiper.js', array(), '1.0.0', true );
    wp_enqueue_script( 'magnific-popup-min-js', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'popup-order-js', get_template_directory_uri() . '/js/popup-order.js', array(), '1.0.0', true );

    if ( is_page_template('template-news.php') ) {
        wp_enqueue_script( 'filter-js', get_template_directory_uri() . '/js/filter.js', array(), '1.0.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'faraday_scripts' );




/**
 * Woocommerce theme support
 */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}




/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
    require_once __DIR__ . '/inc/metaboxes/single-product.php';
    require get_template_directory() . '/woocommerce/includes/wc-functions.php';
}




/**
 * Add theme functions includes and metaboxes
 */
require_once __DIR__ . '/functions-includes/custom-post-type-projects.php';
require_once __DIR__ . '/functions-includes/popup-product-order.php';
require_once __DIR__ . '/functions-includes/update-off.php';

require_once __DIR__ . '/inc/option.php';
require_once __DIR__ . '/inc/metaboxes/category-equipment.php';
require_once __DIR__ . '/inc/metaboxes/home-page.php';
require_once __DIR__ . '/inc/metaboxes/contact.php';
require_once __DIR__ . '/inc/metaboxes/about-us.php';
require_once __DIR__ . '/inc/metaboxes/single-project.php';




add_theme_support( 'post-thumbnails' );




/**
 * Register nav menu
 */
function faraday_register_nav_menu() {
  register_nav_menu( 'header', __('Верхнее меню') );
  register_nav_menu( 'footer', __('Нижнее меню') );
}
add_action( 'after_setup_theme', 'faraday_register_nav_menu' );





/**
 * Remove <span> in cotact form 7
 */
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    return $content;
});




/**
 * Add upload SVG type
 */
function faraday_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'faraday_mime_types');





/**
 * Remove editor for front page
 */
add_action( 'admin_init', 'faraday_hide_editor_front_page' );

function faraday_hide_editor_front_page() {

	if ( isset($_GET['post']) ) {

		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

	    if( !isset( $post_id ) ) return;

	    $template_file = get_post_meta($post_id, '_wp_page_template', true);

	    if($post_id == 2 || $template_file == 'template-category-equipment.php' ){   // $post_id == 2 this is id front page
	        remove_post_type_support('page', 'editor');
	    }
	}
}




/** 
 * Pagination
 */
function faraday_pagination($pages = '', $range = 2) {  
  
     $showitems = $range + 1;  
     global $paged;

     if(empty($paged)) $paged = 1;
 
     if($pages == ''){
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages){
             $pages = 1;
         }
     }   
 
     if( 1 != $pages ){
        
         echo "<div class=\"pagination\">";

         /*if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
            echo "<a href='".get_pagenum_link(1)."' class=\"pagination-item\">&laquo; First</a>";*/

         if($paged > 1 && $showitems < $pages) 
            echo "<a href='".get_pagenum_link($paged - 1)."' class=\"pagination-item\">&larr;</a>";
 
         for ($i=1; $i <= $pages; $i++){

             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){

                 echo ($paged == $i)? "<span class=\"pagination-item active\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"pagination-item\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) 
            echo "<a href=\"".get_pagenum_link($paged + 1)."\" class=\"pagination-item\">&rarr;</a>"; 

         /*if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
            echo "<a href='".get_pagenum_link($pages)."' class=\"pagination-item\">&rarr;</a>";*/

         echo "</div>\n";
     }
}




/** 
 * Reorder comment fields
 */
add_filter('comment_form_fields', 'faraday_reorder_comment_fields' );

function faraday_reorder_comment_fields( $fields ){

    $new_fields = array(); 
    $myorder    = array('author', 'comment', 'email'); 

    foreach( $myorder as $key ){
        $new_fields[ $key ] = $fields[ $key ];
        unset( $fields[ $key ] );
    }

    if( $fields )
        foreach( $fields as $key => $val )
            $new_fields[ $key ] = $val;

    return $new_fields;
}





/** 
 * Redirect to page search
 */
if ( isset($_POST['appointment']) && !empty($_POST['appointment']) ) {

    wp_redirect( get_site_url().'/search/?appointment='.$_POST['appointment'] ); 
    exit;
}




/** 
 * Get page PERMALINK by Page Template
 */
function get_page_PERMALINK_by_template_name($template_name){

    global $wpdb;
    $page_ID        = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_value = '$template_name' AND meta_key = '_wp_page_template'");
    $page_PERMALINK = get_permalink($page_ID);
    
    return $page_PERMALINK;
}
