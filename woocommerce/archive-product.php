<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<div class="search-result-section">
        <div class="container">

            <?php if ( have_posts() ) : ?>

                <h2 id="search_product"><?php single_term_title(); ?></h2>

                <?php while ( have_posts() ) : the_post(); ?>
                	<?php global $product; ?>
                    <div class="category-item">
                        <div class="category-item-img">
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="image">
                            </a>
                        </div>
                        <!-- /.category-item-img -->
                        <h2>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <span class="category-item-price"><?php echo wc_price( $product->get_regular_price() ); ?></span>
                            <!-- /.category-item-price -->
                        </h2>
                        <div class="category-item-description">
                            <?php echo substr($product->get_description(), 0, 140); ?>...
                        </div>
                        <!-- /.category-item-description -->
                        <div class="category-item-info">
                        	<?php echo $product->get_short_description(); ?>
                        </div>
                        <!-- /.category-item-info -->
                    </div>

                <?php endwhile; ?>

            <?php else: ?>

                <h2>Обладнання не знайдено.</h2>

            <?php endif; ?>

            <?php //faraday_pagination($query->max_num_pages); ?>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.search-result-section -->

<?php get_footer( 'shop' ); ?>
