<?php global $product; ?>
<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
	<p><?php esc_html_e( 'Код товару: ', 'woocommerce' ); ?><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></p>
<?php endif; ?>