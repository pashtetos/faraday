<?php global $product; ?>
<div class="remodal modal-buy" data-remodal-id="modalBuy" data-remodal-options="hashTracking:false">
    <div class="modal-header">
        <span class="close" aria-label="Закрыть" data-remodal-action="close"><span></span></span>
        <h2>Придбати</h2>
        <p><?php the_title(); ?></p>
    </div>
    <!-- /.modal-header -->
    <div class="modal-body">
        <form id="order_product" name="order_product" method="post">
            <label for="name">Ваше ім'я*</label>
            <div class="icon-user">
                <input type="text" id="name" name="name" required>
            </div>
            <label for="tel">Ваш телефон*</label> 
            <div class="icon-phone">
                <input type="tel" id="tel" name="tel" required>
            </div>
            <label for="email">E-mail</label>
            <div class="icon-mail">
                <input type="text" id="email" name="email">
            </div>
            <div class="styled-checkbox">
<!--                <input name="place_shipping" type="checkbox" id="placeCheck">-->
<!--                <label for="placeCheck">Вказати місце доставки</label>-->
                <p>Вказати місце доставки</p>
            </div>

<!--            <div class="place-item">-->
<!--                <p>Вказати місце доставки</p>-->
<!--            </div>-->
            <!-- /.place-item -->

            <div id="place_delivery">
                <label for="place">Місто*</label>
                <div class="modal-input">
                    <input type="text" id="place">
                </div>

                <label for="np">№ відділення «Нової Пошти»*</label>
                <div class="modal-input">
                    <input type="text" id="np" name="np">
                </div>
            </div>

           <input type="hidden" name="product_link" value="<?php the_permalink(); ?>">
           <input type="submit" name="submit" class="btn btn-default" value="Відправити заявку">
        </form>
    </div>
    <div class="modal-footer">
        <!-- <button data-remodal-action="confirm" id="submit_order" class="btn btn-default">Відправити заявку</button>  -->
        <a data-remodal-action="cancel" >Відмінити</a>
    </div>
</div>