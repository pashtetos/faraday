<?php global $product; ?>
<div class="product-slider">
	<div class="gallery-top">
	    <div class="swiper-wrapper">

	    	<?php if ( has_post_thumbnail() ): ?>
				<div class="swiper-slide">
	                <?php echo get_the_post_thumbnail( $product->get_id() ); ?>
	            </div>
			<?php endif;  ?>
			<?php 
			    $attachment_ids = $product->get_gallery_image_ids();

			    foreach( $attachment_ids as $attachment_id ):
			        $image_link = wp_get_attachment_url( $attachment_id );
			        echo '<div class="swiper-slide">';
			        	echo '<img src="'.$image_link.'">';
			        echo '</div>';
			    endforeach;
			?>

	    </div>
	    <div class="swiper-button-next"></div>
	    <!-- /.swiper-button-next -->
	    <div class="swiper-button-prev"></div>
	    <!-- /.swiper-button-prev -->
	</div>
	<div class="gallery-thumbs">
	    <div class="swiper-wrapper">
	        
	        <?php if ( has_post_thumbnail() ): ?>
				<div class="swiper-slide">
	                <?php echo get_the_post_thumbnail( $product->get_id() ); ?>
	            </div>
			<?php endif;  ?>
			<?php 
			    $attachment_ids = $product->get_gallery_image_ids();

			    foreach( $attachment_ids as $attachment_id ):
			        $image_link = wp_get_attachment_url( $attachment_id );
			        echo '<div class="swiper-slide">';
			        	echo '<img src="'.$image_link.'">';
			        echo '</div>';
			    endforeach;
			?>

	    </div>
	</div>
</div>