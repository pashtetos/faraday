<?php global $product; ?>

<?php if( $product->get_stock_status() == 'outofstock' ): ?>
	<span class="info">Товар під замовлення: до 10 днів</span>
<?php elseif( $product->get_stock_status() == 'instock' ): ?>
	<span class="info-available product-missing">Товар є в наявності</span>
<?php endif; ?>