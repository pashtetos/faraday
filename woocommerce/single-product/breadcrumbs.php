<?php 
global $product; 

$args = array(
	'delimiter'   => '',
	'wrap_before' => '<ul class="breadcrumbs">',
	'wrap_after'  => '</ul>',
	'before'      => '<li class="breadcrumbs-item">',
	'after'       => '</li>',
	'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
); 

woocommerce_breadcrumb( $args ); ?>