<ul class="product-delivery">
    <li class="delivery">
        <img src="<?php echo faraday_get_option('delivery_img'); ?>" alt="image">
        <strong><?php echo faraday_get_option('delivery_title'); ?></strong>
        <p><?php echo faraday_get_option('delivery_desc'); ?></p>
    </li>
    <!-- /.delivery -->
    <li class="payment">
        <img src="<?php echo faraday_get_option('payment_img'); ?>" alt="image">
        <strong><?php echo faraday_get_option('payment_title'); ?></strong>
        <p><?php echo faraday_get_option('payment_desc'); ?></p>
    </li>
    <li class="assembling">
        <img src="<?php echo faraday_get_option('assembling_img'); ?>" alt="image">
        <strong><?php echo faraday_get_option('assembling_title'); ?></strong>
        <span><?php echo faraday_get_option('assembling_subtitle'); ?></span>
        <p><?php echo faraday_get_option('assembling_desc'); ?></p>
    </li>
</ul>
<!-- /.product-delivery -->