<?php global $product; ?>

<style type="text/css">
    .woocommerce p.stars a {
        font-size: 30px;
        color: #FF9800;
    }
    .woocommerce #respond input#submit {
        background: #d23d3d;
        font-family: Roboto, sans-serif, arial;
        font-weight: 700;
        min-width: 290px;
        padding: 0 15px;
        height: 48px;
        line-height: 48px;
    }
</style>

<?php if (comments_open()): ?>

<div class="remodal modal-review" data-remodal-id="modalReview">
    <div class="modal-header">
        <span class="close" aria-label="Закрыть" data-remodal-action="close"><span></span></span>

        <h2>Додати відгук</h2>
    </div>
    
    <div class="modal-body" style="padding-bottom: 0;">
        <?php
        $commenter = wp_get_current_commenter();

        $comment_form = array(
            'title_reply'          => have_comments() ? __( 'Add a review', 'woocommerce' ) : '',
            'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
            'title_reply_before'   => '',
            'title_reply_after'    => '',
            'comment_notes_after'  => '',
            'comment_notes_before' => '',
            'fields'               => array(
                'author' => '<label for="author">Ваше ім’я*</label>'.
                            '<div class="icon-user">'.
                                '<input type="text" id="author" name="author" value="'.esc_attr( $commenter['comment_author'] ).'" size="30" aria-required="true" required>'.
                            '</div>',
                'email'  => '<label for="email">Ваш E-mail (ніхто бачити небуде) *</label>'.
                            '<div class="icon-mail">'.
                                '<input id="email" name="email" type="email" value="'.esc_attr( $commenter['comment_author_email'] ).'" size="30" aria-required="true" required>'.
                            '</div>',
            ),
            'label_submit'  => 'Відправити заявку',
            'class_submit'  => 'btn btn-default',
            'id_submit'     => 'btn-style',
            'logged_in_as'  => '',
            'comment_field' => '',
        );

        if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
            $comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a review.', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
        }

        if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
            $comment_form['comment_field'] = 
                '<div class="comment-form-rating">'.
                    '<p>Ваша оцінка:</p>'.
                    '<select name="rating" id="rating" aria-required="true" required>
                        <option value="">'  . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
                        <option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
                        <option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
                        <option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
                        <option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
                        <option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
                    </select>'.
                '</div>';
        }

        $comment_form['comment_field'] .= '<label for="review">Ваш відгук</label>'.
                                          '<textarea id="comment" name="comment" cols="30" rows="10" aria-required="true" required></textarea>';

        comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
        ?>
    </div>

    <div class="modal-footer">
        <a data-remodal-action="cancel" >Відмінити</a>
    </div>

</div>

<?php endif; ?>