<?php global $product; ?>

<div class="reviews-section">
    <div class="container">
        <div class="reviews-wrap">

            <?php wc_get_template_part( 'single-product/completed-projects' ); ?>

            <div class="reviews-block">
             
                    <?php $args = array(
                        'orderby' => 'comment_date',
                        'order'   => 'DESC',
                        'post_id' => $product->get_id(),
                        'status'  => 'approve'
                    ); ?>

                    <?php if( $comments = get_comments( $args ) ): ?>

                        <h2>Відгуки</h2>

                        <?php foreach( $comments as $comment ): ?>

                            <?php $comm_link = get_comment_link( $comment->comment_ID ); ?>
                            <?php $d = "l, F jS, Y"; $comment_date = get_comment_date( $d, $comment->comment_ID ); ?>

                            <div class="review-item">
                                <a href="id="<?php echo $comm_link; ?>""></a>
                                <h3><?php echo $comment->comment_author; ?></h3>
                                <div class="review-rating">
                                    <div class="stars" style="color: #FF9800;">
                                        <?php echo wc_get_rating_html( get_comment_meta( $comment->comment_ID, 'rating', true ) ); ?>
                                    </div>
                                    <span><?php echo $comment_date; ?></span>
                                </div>
                                <p><?php echo $comment->comment_content; ?></p>
                                <!-- <a href="#">Подробнее</a> -->
                            </div>
                            <!-- /.review-item -->

                        <?php endforeach; ?>

                    <?php else : ?>

                        <h2>Відгуків ще немає</h2>

                    <?php endif; ?>

                <button class="btn btn-secondary" data-remodal-target="modalReview">Додати відгук</button>

                <!-- /.review-item -->

            </div>
            <!-- /.reviews-block -->
        </div>
        <!-- /.reviews-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.reviews-section -->