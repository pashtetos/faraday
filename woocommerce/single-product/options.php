<?php 
global $product; 
$options = get_post_meta( $product->get_id(), 'faraday_single_product_repeat_option', true ); ?>

<?php if( !empty($options) ): ?>
	<div class="product-options">
	    <p>Додаткові опції:</p>
	    <ul>
	        <?php foreach( (array)$options as $option ): ?>
	            <li><?php echo $option['faraday_single_product_repeat_option_text']; ?></li>
	        <?php endforeach; ?>
	    </ul>
	</div>  
	<!-- /.product-options -->
<?php endif; ?>