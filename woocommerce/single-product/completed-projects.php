<?php  
$args = array(
    'orderby'        => 'rand',
    'post_type'      => 'product',
    'posts_per_page' => 3
);

$loop = new WP_Query( $args ); ?>

<?php if( $loop->have_posts() ): ?>

    <div class="reviews-photo">
        <h2>Фото реалізованих проектів</h2>

        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="reviews-img">
                <a class="image-link" href="<?php the_post_thumbnail_url(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="image"></a>
            </div>
            <!-- /.reviews-img -->
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
    </div>
    <!-- /.reviews-photo -->
    
<?php endif; ?>