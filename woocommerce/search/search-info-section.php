<?php 
global $query; 
$word = get_num_ending( $query->found_posts, array('товар', 'товари', 'товарів') ); 
?>

<div class="search-info-section">
    <div class="container">
        <h1>
            <?php printf( __( 'По вашому запиту знайдено %s '.$word, 'faraday' ), $query->found_posts ); ?>
            <?php if( $query->found_posts != 0 ): ?>
                <a href="#search_product" class="btn btn-default" id="to-products">Перейти до знайдених товарів</a>
            <?php endif; ?>
        </h1>

        <?php if( $_GET['appointment'] == 'dlya-zakladu' ): ?>

            <?php wc_get_template_part( 'search/dlya-zakladu' ); ?>

        <?php elseif( $_GET['appointment'] == 'merezha-platnih-zaryadnih-stantsij' ): ?>

            <?php wc_get_template_part( 'search/merezha-platnih-zaryadnih-stantsij' ); ?>

        <?php elseif( $_GET['appointment'] == 'dlya-privatnih-budinkiv' ): ?>

            <?php wc_get_template_part( 'search/dlya-privatnih-budinkiv' ); ?>

        <?php elseif( $_GET['appointment'] == 'dlya-bagatokvartirnih-budinkiv' ): ?>

            <?php wc_get_template_part( 'search/dlya-bagatokvartirnih-budinkiv' ); ?>

        <?php endif; ?>

    </div>
</div>