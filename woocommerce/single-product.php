<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php wc_get_template_part( 'single-product/popup-success' ); ?>

<div class="product-section">
    <div class="container">

        <?php wc_get_template_part( 'single-product/breadcrumbs' ); ?>

        <div class="product-title">
            <h1><?php the_title(); ?></h1>

            <?php wc_get_template_part( 'single-product/sku' ); ?>
        </div>
        <!-- /.product-title -->

        <div class="product-block">

            <?php wc_get_template_part( 'single-product/gallery' ); ?>

            <div class="product-price">

                <?php wc_get_template_part( 'single-product/price' ); ?>

                <?php wc_get_template_part( 'single-product/options' ); ?>

                <button class="btn btn-default" data-remodal-target="modalBuy">Замовити в один клік</button>
                <!-- /.btn brn-default -->

                <?php wc_get_template_part( 'single-product/available' ); ?>

                <?php wc_get_template_part( 'single-product/features' ); ?>

            </div><!-- /.product-price -->

            <?php wc_get_template_part( 'single-product/delivery' ); ?>

        </div><!-- /.product-slider -->

    </div>
    <!-- /.product-block -->
</div>
<!-- /.container -->

<!-- /.product-section -->
<div class="description-section">
    <div class="container">
        <h2>Опис:</h2>
		<?php echo $product->get_description(); ?>

        <?php wc_get_template_part( 'single-product/attributes' ); ?>

    </div>
    <!-- /.container -->
</div>
<!-- /.description-section -->

<?php wc_get_template_part( 'single-product/reviews' ); ?>

<?php wc_get_template_part( 'single-product/popup-product-order' ); ?>

<?php wc_get_template_part( 'single-product/popup-review' ); ?>

<?php endwhile; // end of the loop. ?>

<?php get_footer( 'shop' );