<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


require get_template_directory() . '/woocommerce/includes/taxonomy-appointment.php';
require get_template_directory() . '/woocommerce/includes/radio-appointment.php';


/** 
 * Get the right word ending
 */ 
function get_num_ending($number, $ending_arr) {

    $number = $number % 100; 

    if ($number >= 11 && $number <= 19) { 
        $ending = $ending_arr[2]; 
    } else { 
        $i = $number % 10; 
        switch ($i) { 
            case (1): $ending = $ending_arr[0]; 
                break; 
            case (2): 
            case (3): 
            case (4): $ending = $ending_arr[1]; 
                break; 
            default: $ending = $ending_arr[2]; 
        } 
    } 

    return $ending; 
}




/** 
 * Greate metabox taxonomy select (left block)
 */ 
add_action('add_meta_boxes', 'product_cat_1_add_meta_box', 10, 2);

function product_cat_1_add_meta_box($post_type, $post) {

  if($post->ID == 2){
    remove_meta_box( 'tagsdiv-types-1', 'page', 'normal' );
    add_meta_box( 'tagsdiv-types-1', 'Вибір Батьківської Категорії ( лівий блок )', 'product_cat_1_meta_box', 'page', 'normal' );
  }
}


function product_cat_1_meta_box($post) {

    $tax_name = 'product_cat';
    $taxonomy = get_taxonomy($tax_name);
    ?>
    <div class="tagsdiv" id="<?php echo $tax_name; ?>">
        <div class="jaxtag">
        <?php 
        wp_nonce_field( plugin_basename( __FILE__ ), 'types_noncename' );
        /*$type_IDs = wp_get_object_terms( $post->ID, $tax_name, array('fields' => 'ids') );*/
        (get_post_meta( $post->ID, '_product_cat_1' )) ? $type_IDs = get_post_meta( $post->ID, '_product_cat_1' ) : $type_IDs = 0;
        wp_dropdown_categories('taxonomy='.$tax_name.'&depth=1&hide_empty=0&hierarchical=1&name=product_cat_1&show_option_none=Вибір Категорії&selected='.$type_IDs[0]); ?>
        </div>
    </div>
    <?php
}


function product_cat_1_save_postdata( $post_id ) {

  if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $post_id ) ) 
      return;

  if ( !wp_verify_nonce( $_POST['types_noncename'], plugin_basename( __FILE__ ) ) )
      return;

  if ( 'page' == $_POST['post_type'] ) {

    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else{

    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  }

  $type_ID = $_POST['product_cat_1'];
  $type    = ( $type_ID > 0 ) ? get_term( $type_ID, 'product_cat' )->slug : NULL;

  /*wp_set_object_terms(  $post_id , $type, 'product_cat' );*/
  update_post_meta( $post_id, '_product_cat_1', $type_ID );
}
add_action( 'save_post', 'product_cat_1_save_postdata' );




/** 
 * Greate metabox taxonomy select (right block)
 */ 
add_action('add_meta_boxes', 'product_cat_2_add_meta_box', 10, 2);

function product_cat_2_add_meta_box($post_type, $post) {

  if($post->ID == 2){
    remove_meta_box( 'tagsdiv-types-2', 'page', 'normal' );
    add_meta_box( 'tagsdiv-types-2', 'Вибір Батьківської Категорії ( правий блок )', 'product_cat_2_meta_box', 'page', 'normal' );
  }
}

/* Prints the taxonomy box content */
function product_cat_2_meta_box($post) {

    $tax_name = 'product_cat';
    $taxonomy = get_taxonomy($tax_name);
    ?>
    <div class="tagsdiv" id="<?php echo $tax_name; ?>">
        <div class="jaxtag">
        <?php 
        wp_nonce_field( plugin_basename( __FILE__ ), 'types_noncename' );
        /*$type_IDs = wp_get_object_terms( $post->ID, $tax_name, array('fields' => 'ids') );*/
        (get_post_meta( $post->ID, '_product_cat_2' )) ? $type_IDs = get_post_meta( $post->ID, '_product_cat_2' ) : $type_IDs = 0;
        wp_dropdown_categories('taxonomy='.$tax_name.'&depth=1&hide_empty=0&hierarchical=1&name=product_cat_2&show_option_none=Вибір Категорії&selected='.$type_IDs[0]); ?>
        </div>
    </div>
    <?php
}


function product_cat_2_save_postdata( $post_id ) {

  if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $post_id ) ) 
      return;

  if ( !wp_verify_nonce( $_POST['types_noncename'], plugin_basename( __FILE__ ) ) )
      return;

  if ( 'page' == $_POST['post_type'] ) {

    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else{

    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  }

  $type_ID = $_POST['product_cat_2'];
  $type    = ( $type_ID > 0 ) ? get_term( $type_ID, 'product_cat' )->slug : NULL;

  /*wp_set_object_terms(  $post_id , $type, 'product_cat' );*/
  update_post_meta( $post_id, '_product_cat_2', $type_ID );
}
add_action( 'save_post', 'product_cat_2_save_postdata' );