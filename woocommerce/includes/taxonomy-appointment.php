<?php

/**
 * Create taxonomy appointment
 */
function faraday_appointment_taxonomy() {

	$labels = array(
		'name'					=> _x( 'Назначения', 'Taxonomy Назначения', 'faraday' ),
		'singular_name'			=> _x( 'Назначение', 'Taxonomy Назначение', 'faraday' ),
		'search_items'			=> __( 'Поиск Назначения', 'faraday' ),
		'popular_items'			=> __( 'Популярные Назначения', 'faraday' ),
		'all_items'				=> __( 'Все Назначения', 'faraday' ),
		'parent_item'			=> __( 'Родительское Назначение', 'faraday' ),
		'parent_item_colon'		=> __( 'Родительское Назначение', 'faraday' ),
		'edit_item'				=> __( 'Edit Назначение', 'faraday' ),
		'update_item'			=> __( 'Обновить Назначение', 'faraday' ),
		'add_new_item'			=> __( 'Добавить Новое Назначение', 'faraday' ),
		'new_item_name'			=> __( 'Новое Имя Назначения', 'faraday' ),
		'add_or_remove_items'	=> __( 'Добавить или Удалить Назначение', 'faraday' ),
		'choose_from_most_used'	=> __( 'Choose from most used faraday', 'faraday' ),
		'menu_name'				=> __( 'Назначения', 'faraday' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'appointment', array( 'product' ), $args );
}

add_action( 'init', 'faraday_appointment_taxonomy' );