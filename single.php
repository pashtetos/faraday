<?php get_header(); ?>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <?php $fname = get_the_author_meta('first_name');
              $lname = get_the_author_meta('last_name'); ?>

        <div class="news-single-section">
            <div class="container">


                <ul class="breadcrumbs">
                    <li class="breadcrumbs-item">
                        <a href="<?php echo site_url(); ?>/news" class="breadcrumbs-link">Новини</a>
                    </li>
                    <li class="breadcrumbs-item">
                        <a href="<?php echo get_the_permalink(); ?>" class="breadcrumbs-link"><?php echo get_the_title(); ?></a>
                    </li>
                </ul>

                <div class="news-single">
                    <div class="news-single-wrap">


                    <!-- /.news-single-wrap -->
                    <div class="new-single-img">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                    </div>
                    <!-- /.new-single-img -->
                    <h1><?php echo get_the_title(); ?></h1>
                    <span class="icon-calendar"><?php the_date(); ?></span><span class="icon-user"><?php echo $fname.' '.$lname; ?></span>
                    </div>
                    <div class="news-single-text">
                        <?php echo get_the_content(); ?>
                    </div>
                    <!-- /.news-single-text -->
                </div>
                <!-- /.news-single -->
            </div>
            <!-- /.container -->
        </div>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>