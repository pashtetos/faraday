<?php
/**
 * Template Name: Поиск
 */

get_header(); ?>

<?php if( isset($_GET['appointment']) ): ?>

    <?php 
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args  = array(
        'paged'          => $paged,
        'post_type'      => 'product',
        'post_status'    => 'publish',
        'posts_per_page' => -1,
        'appointment'    => $_GET['appointment']
    );
    $query = new WP_Query( $args ); ?>

    <?php wc_get_template_part( 'search/search-info-section' ); ?>

    <div class="search-result-section">
        <div class="container">

            <?php if ( $query->have_posts() ): ?>

                <h2 id="search_product">Ось список обладнання, якe вам може підійти:</h2>

                <?php while ( $query->have_posts() ): $query->the_post(); ?>
                    <?php global $product; ?>
                    <div class="category-item">
                        <a class="link-electro-charge" href="<?php the_permalink(); ?>"></a>
                        <div class="category-item-img">
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="image">
                            </a>
                        </div>
                        <!-- /.category-item-img -->
                        <h2>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            <span class="category-item-price"><?php echo wc_price( $product->get_regular_price() ); ?></span>
                            <!-- /.category-item-price -->
                        </h2>
                        <div class="category-item-description">
                            <?php echo substr($product->get_description(), 0, 140); ?>...
                        </div>
                        <!-- /.category-item-description -->
                        <div class="category-item-info">
                            <?php echo $product->get_short_description(); ?>
                        </div>
                        <!-- /.category-item-info -->
                    </div>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else: ?>

                <h2>Обладнання не знайдено.</h2>

            <?php endif; ?>

            <?php //faraday_pagination($query->max_num_pages); ?>

        </div>
        <!-- /.container -->
    </div>




    <div class="remodal modal-info" data-remodal-id="modalInfo">
        <div class="modal-header">
            <span class="close" aria-label="Закрыть" data-remodal-action="close"><span></span></span>
            <h2>Тип конектора <span class="tooltip-btn">?</span></h2>
        </div>
        <!-- /.modal-header -->
        <div class="modal-body">
            <p>Є два типи конекторів для зарядок:</p>
            <ul>
                <li>
                    <h3>1. Кабель </h3>
                    <p>
                        <strong>Переваги:</strong> якщо у зарядній станції вмонтований кабель, то це cпрощує процес
                        зарядки для власника автомобіля - потрібно просто вставити зарядний кабель у автомобіль. Не
                        потрібно ніяких перехідників. <br><br>
                        <strong>Недоліки:</strong> зарядні кабелі не універсальні. Якщо у зарядки кабель типу Type1, то
                        цим кабелем не зможуть зарядитися деякі електромобілі, напр: Tesla, Renault Zoe, тощо.
                    </p>
                </li>
                <li>
                    <h3>2. Розетка </h3>
                    <p>
                        <strong>Переваги:</strong> розетка - це універсальний конектор. Більшість електромобілістів
                        мають власні перехідники, тому встановивши зарядку із розеткою Type2, до неї зможе підключитися
                        більшість електромобілів.<br><br>
                        <strong>Недоліки:</strong> основний недолік розеток це те, що власникам електромобіля для того,
                        щоб приєднатися до зардяки, потрібно діставати власний кабель.
                    </p>
                </li>
            </ul>
        </div>
        <!-- /.modal-body -->
    </div>

<?php endif; ?>

<?php get_footer(); ?>