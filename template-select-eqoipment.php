<?php
/**
 * Template Name: Выбор Товара
 */
?>

<?php get_header(); ?>

	<div class="select-equipment-section">
	    <div class="container">
	        <h1>Зараз ми підберемо для вас обладнання!</h1>
	        <form class="equipment-form" action="" name="search_eqoipment" method="post">
	            <div class="select-equipment-block">
	                <div class="equipment-item" data-step="1">
	                    <div class="equipment-item-title">
	                        <h3><span>1</span>Зарядна станція вам потрібна:</h3>
	                    </div>
	                    <!-- /.equipment-item-title -->

	                    <div class="equipment-item-inner">
	                        <!-- /.change-station -->
	                        <div class="styled-radio">
	                            <input type="radio" class="equipment-select" data-equipment="business" id='business' name="warranty_case">
	                            <label for="business">Для бізнесу</label>
	                        </div>
	                        <div class="styled-radio">
	                            <input type="radio" class="equipment-select" data-equipment="personal" id='personal-use' name="warranty_case">
	                            <label for="personal-use">Для особистого використання</label>
	                        </div>
	                    </div>
	                </div>
	                <div class="equipment-item business-item">
	                    <div class="equipment-item-title">
	                        <h3><span>2</span>Який саме формат вас цікавить?</h3>
	                    </div>
	                    <!-- /.equipment-item-title -->
	                    <div class="equipment-item-inner">
	                        <!-- /.change-station -->
	                        <div class="styled-radio">
	                            <input type="radio" class="format-station" id='business2' name="appointment" value="dlya-zakladu">
	                            <label for="business2">У мене ресторан/офіс/тощо. Хочу там поставити зарядку для електромобілів</label>
	                        </div>
	                        <div class="styled-radio">
	                            <input type="radio" class="format-station" id='personal-use2' name="appointment" value="merezha-platnih-zaryadnih-stantsij">
	                            <label for="personal-use2">Хочу створити мережу платних зарядних станцій для електромобілів</label>
	                        </div>
	                    </div>
	                </div>
	                <!--<div class="equipment-item personal-item">-->
	                    <!--<div class="equipment-item-title">-->
	                        <!--<h3><span>2</span>Який у вас електромобіль?</h3>-->
	                    <!--</div>-->
	                    <!--&lt;!&ndash; /.equipment-item-title &ndash;&gt;-->
	                    <!--<div class="equipment-item-inner">-->
	                        <!--&lt;!&ndash; /.change-station &ndash;&gt;-->
	                        <!--<div class="select-inline">-->
	                            <!--<label>Марка:</label>-->
	                            <!--<select class="nice-select brand-select">-->
	                                <!--<option value="" data-display="" disabled selected></option>-->
	                                <!--<option value="2">Nissan</option>-->
	                                <!--<option value="3">Chevrolet</option>-->
	                                <!--<option value="4">Tesla</option>-->
	                                <!--<option value="5">Zaz</option>-->
	                            <!--</select>-->
	                        <!--</div>-->
	                        <!--&lt;!&ndash; /.select-inline &ndash;&gt;-->
	                        <!--<div class="select-inline">-->
	                            <!--<label>Модель:</label>-->
	                            <!--<select class="nice-select model-select disabled">-->
	                                <!--<option value="" data-display="" disabled selected></option>-->
	                                <!--<option value="1">Leaf</option>-->
	                                <!--<option value="2">Bolt</option>-->
	                                <!--<option value="3">Model S</option>-->
	                                <!--<option value="4">Sens</option>-->
	                            <!--</select>-->
	                        <!--</div>-->
	                    <!--</div>-->
	                <!--</div>-->
	                <div class="equipment-item personal-item">
	                    <div class="equipment-item-title">
	                        <h3><span>2</span>Уточніть, де ви проживаєте:</h3>
	                    </div>
	                    <!-- /.equipment-item-title -->
	                    <div class="equipment-item-inner">
	                        <!-- /.change-station -->
	                        <div class="styled-radio">
	                            <input type="radio" class="type-house" id='home1' name="appointment" value="dlya-privatnih-budinkiv">
	                            <label for="home1"> У приватному будинку</label>
	                        </div>
	                        <div class="styled-radio">
	                            <input type="radio" class="type-house" id='home2' name="appointment" value="dlya-bagatokvartirnih-budinkiv">
	                            <label for="home2">В багатоквартирному будинку</label>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- /.select-equipment-block -->
	            
	            <div class="progress">
	              <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
	              </div>
	            </div>

	            <button class="btn btn-default equipment-submit" disabled>Підібрати обладнання</button>
	        </form>

	    </div>
	    <!-- /.container -->
	</div>
	<!-- /.category-equipment -->

<?php get_footer(); ?>