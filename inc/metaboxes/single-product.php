<?php
add_action( 'cmb2_admin_init', 'faraday_single_product_metabox' );

function faraday_single_product_metabox() {

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_single_product_metabox_option',
		'title'         => __( 'Додаткові Опції', 'faraday' ),
		'object_types'  => array( 'product' ), 
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => false, 
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'faraday_single_product_repeat_option',
		'type'        => 'group',
		'options'     => array(
			'group_title'   => __( 'Опція {#}', 'faraday' ), 
			'add_button'    => __( 'Додати опцію', 'faraday' ),
			'remove_button' => __( 'Видалити опцію', 'faraday' ),
			'sortable'      => true, 
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Опція',
		'id'      => 'faraday_single_product_repeat_option_text',
		'type'    => 'text'
	) );




	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_single_product_metabox_item',
		'title'         => __( 'Особливості Продукту', 'faraday' ),
		'object_types'  => array( 'product' ), 
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => 'Текст',
		'id'      => 'faraday_single_product_metabox_wysiwyg',
		'type'    => 'wysiwyg',
		'options' => array(),
	) );

	/*$group_field_id = $cmb->add_field( array(
		'id'          => 'faraday_single_product_repeat_item',
		'type'        => 'group',
		'options'     => array(
			'group_title'   => __( 'Особливість {#}', 'faraday' ), 
			'add_button'    => __( 'Додати Особливість', 'faraday' ),
			'remove_button' => __( 'Видалити Особливість', 'faraday' ),
			'sortable'      => true, 
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Особливість',
		'id'      => 'faraday_single_product_repeat_item_text',
		'type'    => 'text'
	) );*/

}