<?php
add_action( 'cmb2_admin_init', 'faraday_single_project_metabox' );

function faraday_single_project_metabox() {

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_single_project_metabox',
		'title'         => __( 'Зображення проекту (велике)', 'faraday' ),
		'object_types'  => array( 'projects' ), 
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => false, 
	) );

	$cmb->add_field( array(
		'name'    => 'Зображення проекту (велике)',
		'id'      => 'single_project_image',
		'type'    => 'file',
		'options' => array( 'url' => false, ),
		'text'    => array( 'add_upload_file_text' => 'Додати Зображення' ),
		'query_args'   => array( 'type' => 'image' ),
		'preview_size' => 'large',
	) );

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_single_project_metabox_2',
		'title'         => __( 'Додаткові опції', 'faraday' ),
		'object_types'  => array( 'projects' ), 
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => 'Підзаголовок',
		'id'      => 'single_project_comand_subtitle',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name'    => 'Короткий опис',
		'id'      => 'single_project_comand_excerpt',
		'type'    => 'textarea_small'
	) );

}