<?php
add_action( 'cmb2_admin_init', 'faraday_about_us_metabox' );

function faraday_about_us_metabox() {

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_about_us_metabox',
		'title'         => __( 'Інформаційний блок', 'faraday' ),
		'object_types'  => array( 'page' ), 
		'show_on' 		=> array( 'key' => 'page-template', 'value' => 'template-about-us.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => 'Опис',
		'id'      => 'about_us_desc',
		'type'    => 'wysiwyg',
		'options' => array(),
	) );

	$cmb = new_cmb2_box( array(
		'id'            => 'about_us_comand_metabox',
		'title'         => __( 'Команда', 'faraday' ),
		'object_types'  => array( 'page' ), 
		'show_on' 		=> array( 'key' => 'page-template', 'value' => 'template-about-us.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => 'Назва',
		'id'      => 'about_us_comand_title',
		'type'    => 'text'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'about_us_repeat',
		'type'        => 'group',
		// 'description' => __( 'Generates reusable form entries', 'faraday' ),
		'options'     => array(
			'group_title'   => __( 'Член команди {#}', 'faraday' ), 
			'add_button'    => __( 'Додати члена', 'faraday' ),
			'remove_button' => __( 'Видалити члена', 'faraday' ),
			'sortable'      => true, 
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Зображення',
		'id'      => 'about_us_image',
		'type'    => 'file',
		'options' => array( 'url' => false, ),
		'text'    => array( 'add_upload_file_text' => 'Додати Зображення' ),
		'query_args'   => array( 'type' => 'image' ),
		'preview_size' => 'large',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Ім’я',
		'id'      => 'about_us_name',
		'type'    => 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Назва соц. посилання',
		'id'      => 'about_us_soc_name',
		'type'    => 'text'
	) );
	
	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Соц. посилання',
		'id'      => 'about_us_soc_link',
		'type' 	  => 'text_url',
		'protocols' => array( 'http', 'https' ),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => 'Опис',
		'id'   => 'about_us_item_desc',
		'type' => 'textarea_small'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Назва посилання',
		'id'      => 'about_us_link_name',
		'type'    => 'text'
	) );
	
	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Посилання',
		'id'      => 'about_us_link',
		'type' 	  => 'text_url',
		'protocols' => array( 'http', 'https' ),
	) );
}