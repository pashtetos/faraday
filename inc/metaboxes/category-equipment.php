<?php
add_action( 'cmb2_admin_init', 'faraday_category_equipment_metabox' );

function faraday_category_equipment_metabox() {

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_category_equipment_metabox',
		'title'         => __( 'Категорії Товарів', 'faraday' ),
		'object_types'  => array( 'page' ), 
		'show_on' 		=> array( 'key' => 'page-template', 'value' => 'template-category-equipment.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'           => 'Вибір Батьківської Категорії (лівий блок)',
		'id'             => 'category_equipment_cat_1',
		'taxonomy'       => 'product_cat', 
		'type'           => 'taxonomy_select',
	) );

	$cmb->add_field( array(
		'name'           => 'Вибір Батьківської Категорії (правий блок)',
		'id'             => 'category_equipment_cat_2',
		'taxonomy'       => 'product_cat', 
		'type'           => 'taxonomy_select',
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва', 'faraday' ),
		'id'      => 'category_equipment_title',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Опис', 'faraday' ),
		'id'      => 'category_equipment_desc',
		'type'    => 'textarea_small'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Текст посилання', 'faraday' ),
		'id'      => 'category_equipment_name_link',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name' => __( 'Посилання', 'faraday' ),
		'id'   => 'category_equipment_url_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https' ),
	) );

}