<?php
add_action( 'cmb2_admin_init', 'faraday_contact_metabox' );

function faraday_contact_metabox() {

	$cmb = new_cmb2_box( array(
		'id'            => 'faraday_contact_metabox',
		'title'         => __( 'Налаштування шаблону', 'faraday' ),
		'object_types'  => array( 'page' ), 
		'show_on' 		=> array( 'key' => 'page-template', 'value' => 'template-contact.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name' => 'Верхній блок',
		'type' => 'title',
		'id'   => 'contact_title_block_1'
	) );

	$cmb->add_field( array(
		'name'    => 'Назва',
		'id'      => 'contact_title_1',
		'type'    => 'text_medium'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'contact_repeat_1',
		'type'        => 'group',
		'description' => __( 'Генератор телефонів', 'faraday' ),
		'options'     => array(
			'group_title'   => __( 'Телефон {#}', 'faraday' ), 
			'add_button'    => __( 'Додати Телефон', 'faraday' ),
			'remove_button' => __( 'Видалити Телефон', 'faraday' ),
			'sortable'      => true, 
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Телефон',
		'id'      => 'contact_phone_1',
		'type'    => 'text_medium'
	) );

	$cmb->add_field( array(
		'name'    => 'Ім’я',
		'id'      => 'contact_name_1',
		'type'    => 'text_medium'
	) );

	$cmb->add_field( array(
		'name' => 'Нижний блок',
		'type' => 'title',
		'id'   => 'contact_title_block_2'
	) );

	$cmb->add_field( array(
		'name'    => 'Назва',
		'id'      => 'contact_title_2',
		'type'    => 'text_medium'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'contact_repeat_2',
		'type'        => 'group',
		'description' => __( 'Генератор телефонів', 'faraday' ),
		'options'     => array(
			'group_title'   => __( 'Телефон {#}', 'faraday' ), 
			'add_button'    => __( 'Додати Телефон', 'faraday' ),
			'remove_button' => __( 'Видалити Телефон', 'faraday' ),
			'sortable'      => true, 
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'Телефон',
		'id'      => 'contact_phone_2',
		'type'    => 'text_medium'
	) );

	$cmb->add_field( array(
		'name'    => 'Ім’я',
		'id'      => 'contact_name_2',
		'type'    => 'text_medium'
	) );

	$cmb->add_field( array(
		'name' => __( 'Email', 'faraday' ),
		'id'   => 'contact_email',
		'type' => 'text_email',
	) );

	$cmb->add_field( array(
		'name'    => 'Зображення сторінки',
		'id'      => 'contact_img',
		'type'    => 'file',
		'options' => array(
			'url' => false, 
		),
		'text'    => array(
			'add_upload_file_text' => 'Додати зображення' 
		),
		'query_args'   => array( 'type' => 'image' ),
		'preview_size' => 'large',
	) );
}