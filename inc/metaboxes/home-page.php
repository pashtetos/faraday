<?php 

add_filter( 'cmb2_show_on', 'faraday_metabox_include_front_page', 10, 2 );

function faraday_metabox_include_front_page( $display, $meta_box ) {

	if ( ! isset( $meta_box['show_on']['key'] ) ) {
		return $display;
	}

	if ( 'front-page' !== $meta_box['show_on']['key'] ) {
		return $display;
	}

	$post_id = 0;

	// If we're showing it based on ID, get the current ID
	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	} elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}

	if ( ! $post_id ) {
		return false;
	}

	// Get ID of page set as front page, 0 if there isn't one
	$front_page = get_option( 'page_on_front' );

	// there is a front page set and we're on it!
	return $post_id == $front_page;
}






add_action( 'cmb2_admin_init', 'faraday_frontpage_metaboxes' );

function faraday_frontpage_metaboxes() {



	/**
	 * Intro
	**/
	$cmb = new_cmb2_box( array(
		'id'            => 'frontpage_info_metabox',
		'title'         => __( 'Перший блок', 'faraday' ),
		'object_types'  => array( 'page', ), 
		'show_on' 		=> array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => __( 'Заголовок', 'faraday' ),
		'id'      => 'frontpage_title_info',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name' 	  => __( 'Опис', 'faraday' ),
		'id' 	  => 'frontpage_desc_info',
		'type'    => 'wysiwyg',
		'options' => array()
	) );

	$cmb->add_field( array(
        'name'    => 'Зображення',
        'id'      => 'frontpage_img_info',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати Зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );



	 /**
	 * Advantages
	**/
    $cmb = new_cmb2_box( array(
		'id'            => 'frontpage_advantages_metabox',
		'title'         => __( 'Блок Переваг', 'faraday' ),
		'object_types'  => array( 'page', ), 
		'show_on' 		=> array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва блока', 'faraday' ),
		'id'      => 'frontpage_advantages_block_title',
		'type'    => 'text'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'frontpage_advantages__group',
		'type'        => 'group',
		'description' => __( 'Генератор Переваг', 'faraday' ),
		'options'     => array(
			'group_title'   => __( 'Перевага {#}', 'faraday' ), 
			'add_button'    => __( 'Додати Перевагу', 'faraday' ),
			'remove_button' => __( 'Видалити Перевагу', 'faraday' ),
			'sortable'      => true, 
			'closed'     	=> true,
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
        'name'    => 'Зображення',
        'id'      => 'frontpage_advantages_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати Зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => 'Заголовок',
		'id'   => 'frontpage_advantages_title',
		'type' => 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name' 	  => __( 'Опис', 'faraday' ),
		'id' 	  => 'frontpage_advantages_desc',
		'type'    => 'wysiwyg',
		'options' => array()
	) );




	/**
	 * Friends
	**/
	$cmb = new_cmb2_box( array(
		'id'            => 'frontpage_friends_metabox',
		'title'         => __( 'Блок Друзів', 'faraday' ),
		'object_types'  => array( 'page', ), 
		'show_on' 		=> array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва блоку', 'faraday' ),
		'id'      => 'frontpage_friends_title',
		'type'    => 'text'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'          => 'frontpage_friends_gallery_group',
		'type'        => 'group',
		'description' => __( 'Генератор Слайдів', 'faraday' ),
		'options'     => array(
			'group_title'   => __( 'Слайд {#}', 'faraday' ), 
			'add_button'    => __( 'Додати Слайд', 'faraday' ),
			'remove_button' => __( 'Видалити Слайд', 'faraday' ),
			'sortable'      => true, 
			'closed'     	=> true,
		),
	) );

	$cmb->add_group_field( $group_field_id, array(
        'name'    => 'Зображення',
        'id'      => 'frontpage_friends_gallery_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати Зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Посилання', 'faraday' ),
		'id'   => 'frontpage_friends_gallery_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https' ),
	) );




	/**
	 * Info
	**/
	$cmb = new_cmb2_box( array(
		'id'            => 'frontpage_news_metabox',
		'title'         => __( 'Інформаційний Блок', 'faraday' ),
		'object_types'  => array( 'page', ), 
		'show_on' 		=> array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва блоку', 'faraday' ),
		'id'      => 'frontpage_news_projects_title',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва блоку', 'faraday' ),
		'id'      => 'frontpage_news_projects_subtitle',
		'type'    => 'textarea_small'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Вибір реалізованих проектів для виведення в блоці', 'faraday' ),
		'id'      => 'frontpage_news_projects_attached_posts',
		'type'    => 'custom_attached_posts',
		'column'  => true, 
		'options' => array(
			'show_thumbnails' => true, 
			'filter_boxes'    => true, 
			'query_args'      => array(
				'posts_per_page' => -1,
				'post_type'      => 'projects',
			), 
		),
	) );

	$cmb->add_field( array(
		'name'    => __( 'Назва блоку', 'faraday' ),
		'id'      => 'frontpage_news_news_title',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Вибір Новин для виведення в блоці', 'faraday' ),
		'id'      => 'frontpage_news_news_attached_posts',
		'type'    => 'custom_attached_posts',
		'column'  => true, 
		'options' => array(
			'show_thumbnails' => true, 
			'filter_boxes'    => true, 
			'query_args'      => array(
				'posts_per_page' => -1,
				'post_type'      => 'post',
			), 
		),
	) );



	/**
	 * Products categories
	**/
	$cmb = new_cmb2_box( array(
		'id'            => 'frontpage_categories_metabox',
		'title'         => __( 'Категории Товаров', 'faraday' ),
		'object_types'  => array( 'page', ), 
		'show_on' 		=> array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, 
	) );

	// $cmb->add_field( array(
	// 	'name'           => 'Выбор Родительской Категории (левый блок)',
	// 	'id'             => 'frontpage_categories_cat_1',
	// 	'taxonomy'       => 'product_cat', 
	// 	'type'           => 'taxonomy_select',
	// ) );

	// $cmb->add_field( array(
	// 	'name'           => 'Выбор Родительской Категории (правый блок)',
	// 	'id'             => 'frontpage_categories_cat_2',
	// 	'taxonomy'       => 'product_cat', 
	// 	'type'           => 'taxonomy_select',
	// ) );

	$cmb->add_field( array(
		'name'    => __( 'Заглавие', 'faraday' ),
		'id'      => 'frontpage_categories_title',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name'    => __( 'Опис', 'faraday' ),
		'id'      => 'frontpage_categories_desc',
		'type'    => 'textarea_small'
	) );

	// $cmb->add_field( array(
	// 	'name'           => 'Посилання',
	// 	'id'             => 'frontpage_categories_cta_link', 
	// 	'type'           => 'link_picker',
	// 	'split_values'   => true,
	// ) );

	/*$cmb->add_field( array(
		'name'    => __( 'Текст ссылки', 'faraday' ),
		'id'      => 'frontpage_categories_name_link',
		'type'    => 'text'
	) );

	$cmb->add_field( array(
		'name' => __( 'Посилання', 'faraday' ),
		'id'   => 'frontpage_categories_url_link',
		'type' => 'text_url',
		'protocols' => array( 'http', 'https' ),
	) );*/

}