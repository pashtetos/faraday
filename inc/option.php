<?php

function faraday_get_option($key = '', $default = false) {

    if (function_exists('cmb2_get_option')) {

        return cmb2_get_option('faraday_options', $key, $default);
    }

    $opts = get_option('faraday_options', $default);
    $val = $default;
    if ('all' == $key) {
        $val = $opts;
    } elseif (is_array($opts) && array_key_exists($key, $opts) && false !== $opts[$key]) {
        $val = $opts[$key];
    }
    return $val;
}


add_action('cmb2_init', 'faraday_register_theme_options_metabox');
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function faraday_register_theme_options_metabox() {
    
    /**
     * Registers options page menu item and form.
     */
    $cmb_options = new_cmb2_box(array(
        'id'    => 'faraday_option_metabox',
        'title' => esc_html__('Налаштування Теми', 'faraday'),
        'object_types' => array('options-page'),
        'option_key'   => 'faraday_options', // The option key and admin menu page slug.
        'save_button'  => esc_html__('Зберегти', 'faraday'), // The text for the options-page save button. Defaults to 'Save'.
    ));

     /**
     * Contacts options
    **/
    $cmb_options->add_field(array(
        'name' => 'Контакти',
        'id'   => 'contact_title',
        'type' => 'title',
    ));
    $cmb_options->add_field(array(
        'name' => 'Телефон',
        'id'   => 'header_phone_1',
        'type' => 'text',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Іконка телефонного оператора',
        'id'      => 'header_phone_1_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати файл' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb_options->add_field(array(
        'name' => 'Телефон',
        'id'   => 'header_phone_2',
        'type' => 'text',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Іконка телефонного оператора',
        'id'      => 'header_phone_2_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати файл' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );
    $cmb_options->add_field( array(
        'name' => 'Пошта',
        'id'   => 'header_email',
        'type' => 'text_email',
    ) );

     /**
     * Header options
    **/
    $cmb_options->add_field(array(
        'name' => 'Шапка сайту',
        'id'   => 'header_title',
        'type' => 'title',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Логотип',
        'id'      => 'header_logo',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати файл' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb_options->add_field( array(
        'name'    => 'Логотип (мобільного меню)',
        'id'      => 'header_logo_menu',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати файл' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    /**
     * Footer options
    **/
    $cmb_options->add_field(array(
        'name' => 'Підвал сайту',
        'id'   => 'footer_title',
        'type' => 'title',
    ));

    $cmb_options->add_field(array(
        'name' => 'Текст посилання',
        'id'   => 'footer_link_title',
        'type' => 'text',
    ));

    $cmb_options->add_field( array(
        'name' => __( 'Посилання', 'faraday' ),
        'id'   => 'footer_link_link',
        'type' => 'text_url',
        'protocols' => array( 'http', 'https' ),
    ) );

    $cmb_options->add_field(array(
        'name' => 'Текст соц. посилання',
        'id'   => 'footer_soc_title',
        'type' => 'text',
    ));

    $cmb_options->add_field( array(
        'name' => __( 'Соц. посилання', 'faraday' ),
        'id'   => 'footer_soc_link',
        'type' => 'text_url',
        'protocols' => array( 'http', 'https' ),
    ) );

     /**
     * Product delivery
    **/
    $cmb_options->add_field(array(
        'name' => 'Доставка',
        'id'   => 'delivery_name',
        'type' => 'title',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Іконка',
        'id'      => 'delivery_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb_options->add_field(array(
        'name' => 'Назва',
        'id'   => 'delivery_title',
        'type' => 'text',
    ));

    $cmb_options->add_field(array(
        'name' => 'Опис',
        'id'   => 'delivery_desc',
        'type' => 'text',
    ));

    /**
     * Product payment
    **/
    $cmb_options->add_field(array(
        'name' => 'Оплата',
        'id'   => 'payment_name',
        'type' => 'title',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Іконка',
        'id'      => 'payment_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb_options->add_field(array(
        'name' => 'Назва',
        'id'   => 'payment_title',
        'type' => 'text',
    ));

    $cmb_options->add_field(array(
        'name' => 'Опис',
        'id'   => 'payment_desc',
        'type' => 'text',
    ));

    /**
     * Product assembling
    **/
    $cmb_options->add_field(array(
        'name' => 'Монтаж',
        'id'   => 'assembling_name',
        'type' => 'title',
    ));

    $cmb_options->add_field( array(
        'name'    => 'Іконка',
        'id'      => 'assembling_img',
        'type'    => 'file',
        'options' => array( 'url' => false, ),
        'text'    => array( 'add_upload_file_text' => 'Додати зображення' ),
        'query_args'   => array( 'type' => 'image' ),
        'preview_size' => 'large',
    ) );

    $cmb_options->add_field(array(
        'name' => 'Назва',
        'id'   => 'assembling_title',
        'type' => 'text',
    ));

    $cmb_options->add_field(array(
        'name' => 'Підзаголовок',
        'id'   => 'assembling_subtitle',
        'type' => 'text',
    ));

    $cmb_options->add_field(array(
        'name' => 'Опис',
        'id'   => 'assembling_desc',
        'type' => 'text',
    ));
}

echo faraday_get_option('faraday_options'); 