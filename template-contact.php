<?php
/**
 * Template Name: Контакты
 */
?>

<?php get_header(); ?>

<div class="contacts-section">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <div class="contacts-block">
            <div class="contacts-info">
                <p><?php echo get_post_meta( get_the_ID(), 'contact_title_1', true ); ?></p>
                <ul class="contact-item icon-phone">
                	<?php $phones_1 = get_post_meta( get_the_ID(), 'contact_repeat_1', true ); ?>

                	<?php foreach( (array)$phones_1 as $phone_1 ): ?>
                		<?php $phone_tel_1 = str_replace( " ", "", $phone_1['contact_phone_1'] );
    						  $phone_tel_1 = str_replace( array(')', '('), array('',''), $phone_tel_1 ); 
    						  $phone_tel_1 = str_replace( array('–', '–'), array('',''), $phone_tel_1 );  ?>
                    	<li><a href="tel:<?php echo $phone_tel_1; ?>"><?php echo $phone_1['contact_phone_1']; ?></a></li>
                	<?php endforeach; ?>

                    <li><p><?php echo get_post_meta( get_the_ID(), 'contact_name_1', true ); ?></p></li>
                </ul>

                <p><?php echo get_post_meta( get_the_ID(), 'contact_title_2', true ); ?></p>
                <ul class="contact-item icon-phone">
                    <?php $phones_2 = get_post_meta( get_the_ID(), 'contact_repeat_2', true ); ?>

                	<?php foreach( (array)$phones_2 as $phone_2 ): ?>
                		<?php $phone_tel_2 = str_replace( " ", "", $phone_2['contact_phone_2'] );
    						  $phone_tel_2 = str_replace( array(')', '('), array('',''), $phone_tel_2 ); 
    						  $phone_tel_2 = str_replace( array('–', '–'), array('',''), $phone_tel_2 );  ?>
                    	<li><a href="tel:<?php echo $phone_tel_2; ?>"><?php echo $phone_2['contact_phone_2']; ?></a></li>
                	<?php endforeach; ?>

                    <li><p><?php echo get_post_meta( get_the_ID(), 'contact_name_2', true ); ?></p></li>
                </ul>
                <div class="contact-mail icon-mail">
                    <a href="mailto:<?php echo get_post_meta( get_the_ID(), 'contact_email', true ); ?>">
                    	<?php echo get_post_meta( get_the_ID(), 'contact_email', true ); ?>
                    </a>
                </div>
                <!-- /.contact-mail -->
            </div>
            <!-- /.contacts-info -->
            <div class="contacts-img">
                <img src="<?php echo get_post_meta( get_the_ID(), 'contact_img', true ); ?>" alt="image">
            </div>
            <!-- /.contacts-img -->
        </div>
        <!-- /.contacts-block -->
    </div>
    <!-- /.container -->
</div>

<?php get_footer(); ?>