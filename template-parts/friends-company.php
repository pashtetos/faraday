
<div class="friends-company-wrap">
    <div class="container">
        <h2><?php echo get_post_meta( get_the_ID(), 'frontpage_friends_title', true ); ?></h2>
        <!-- /.container -->

        <?php $items = get_post_meta( get_the_ID(), 'frontpage_friends_gallery_group', true ); ?>
        
        <div class="friends-slider swiper-container">
            <div class="swiper-wrapper">

                <?php foreach( (array)$items as $item ): ?>
                    <div class="swiper-slide">
                        <a href="<?php echo $item['frontpage_friends_gallery_link']; ?>">
                            <img src="<?php echo $item['frontpage_friends_gallery_img']; ?>" alt="image">
                        </a>
                    </div>
                <?php endforeach; ?>

            </div>
            <div class="swiper-btn swiper-btn-next"><img src="<?php echo get_template_directory_uri(); ?>/img/right.svg" alt="image"></div>
            <div class="swiper-btn swiper-btn-prev"><img src="<?php echo get_template_directory_uri(); ?>/img/left.svg" alt="image"></div>
            <!-- /.swiper-wrapper -->
        </div>

    </div>    <!-- /.friends-slider -->
</div>
<!-- /.friends-company -->