
<div class="main-info-wrap">
    <div class="container">
        <div class="main-info-item">
            <div class="main-info">
                <h1><?php echo get_post_meta( get_the_ID(), 'frontpage_title_info', true ); ?></h1>
                <?php echo get_post_meta( get_the_ID(), 'frontpage_desc_info', true ); ?>
            </div>
            <div class="main-image">
                <img src="<?php echo get_post_meta( get_the_ID(), 'frontpage_img_info', true ); ?>" alt="img">
            </div>
        </div>
    </div>
</div>