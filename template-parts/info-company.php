
<div class="info-company-wrap">
    <div class="container">
        <div class="info-company">
            <!-- /.info-company -->

            <div class="implemented-projects">
                <h3><?php echo get_post_meta( get_the_ID(), 'frontpage_news_projects_title', true ); ?></h3>
                <p><?php echo get_post_meta( get_the_ID(), 'frontpage_news_projects_subtitle', true ); ?></p>

                <?php $projects_arr = get_post_meta( get_the_ID(), 'frontpage_news_projects_attached_posts', true ); ?>
                <ul class="spend-coming-wrap">
        
                    <?php foreach( (array)$projects_arr as $project ): ?>
                        <?php $proj = get_post( $project ); ?>
                        <li class="spend-coming">
                            <h4>
                                <a href="<?php echo get_the_permalink($proj->ID); ?>">
                                    <?php echo $proj->post_title; ?>
                                </a>
                            </h4>
                            <span><?php echo get_post_meta( $proj->ID, 'single_project_comand_subtitle', true ); ?></span>
                        </li>
                    <?php endforeach; wp_reset_postdata(); ?>

                </ul>
                <!-- /.spend-coming-wrap -->

                <!-- /.spend-coming -->
            </div>

            <div class="news">
                <h3>Новини</h3>

                <?php $news_arr = get_post_meta( get_the_ID(), 'frontpage_news_news_attached_posts', true ); ?>
                <?php foreach( (array)$news_arr as $news ): ?>
                    <?php $post = get_post( $news ); ?>
                    <a href="<?php the_permalink($post->ID); ?>">
                        <?php echo $post->post_title; ?>
                    </a>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>

        </div>
        <!-- /.news -->
        <!-- /.implemented-projects -->
    </div>
    <!-- /.container -->
</div>