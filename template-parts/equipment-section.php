<?php
global $post;
$pages = get_pages(array(
    'meta_key'   => '_wp_page_template',
    'meta_value' => 'template-select-eqoipment.php'
));

$template_id = $pages[0]->ID;

(get_post_meta( $post->ID, '_product_cat_1' )) ? $cat_1_id = get_post_meta( $post->ID, '_product_cat_1' ) : $cat_1_id = 0;
(get_post_meta( $post->ID, '_product_cat_2' )) ? $cat_2_id = get_post_meta( $post->ID, '_product_cat_2' ) : $cat_2_id = 0;

$args_1 = array(
     'taxonomy'     => 'product_cat',
     'parent'       => $cat_1_id[0],
     'hide_empty'   => false
);

$args_2 = array(
     'taxonomy'     => 'product_cat',
     'parent'       => $cat_2_id[0],
     'hide_empty'   => false
);

$cat_1_name     = get_term_by( 'id', $cat_1_id[0], 'product_cat' );
$cat_2_name     = get_term_by( 'id', $cat_2_id[0], 'product_cat' );

$all_categories_1 = get_categories( $args_1 ); 
$all_categories_2 = get_categories( $args_2 ); ?>

<div class="equipment-section">
    <div class="equipment-wrap">
        <div class="charging-station equipment">

            <h3><?php echo $cat_1_name->name; ?></h3>

             <?php foreach ($all_categories_1 as $cat) {

                if($cat->category_parent != 0) {
 
                    $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                    $image        = wp_get_attachment_url($thumbnail_id); ?>

                    <a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>" class="type-station">
                        <img src="<?php echo $image; ?>" alt="image">
                        <p><?php echo $cat->name; ?></p>
                    </a>

                <?php }        
            } ?>
            <!-- /.type-station -->
        </div>

        <div class="accessories equipment">

            <h3><?php echo $cat_2_name->name; ?></h3>

             <?php foreach ($all_categories_2 as $cat) {

                if($cat->category_parent != 0) { 

                    $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                    $image        = wp_get_attachment_url($thumbnail_id); ?>

                    <a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>" class="type-station">
                        <img src="<?php echo $image; ?>" alt="image">
                        <p><?php echo $cat->name; ?></p>
                    </a>

                <?php }        
            } ?>

        </div>
        <!-- /.selection-equipment -->
    </div>

    <div class="selection-equipment">
        <h3><?php echo get_post_meta( $post->ID, 'frontpage_categories_title', true ); ?></h3>
        <p><?php echo get_post_meta( $post->ID, 'frontpage_categories_desc', true ); ?></p>
        <a href="<?php echo get_permalink($template_id); ?>" class="btn btn-default link-btn">
            Підібрати обладнання
        </a>
    </div>

</div>
<!-- /.equipment-wrap -->