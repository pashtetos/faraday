
<div class="advantages-section">
    <div class="container">
        <h3><?php echo get_post_meta( get_the_ID(), 'frontpage_advantages_block_title', true ); ?></h3>
        <div class="advantages-wrap">
            <!-- /.advantages-wrap -->

            <?php $items = get_post_meta( get_the_ID(), 'frontpage_advantages__group', true ); ?>

            <?php foreach( (array)$items as $item ): ?>

                <div class="advantages-item">
                    <!-- /.advantages-wrap -->
                    <div class="advantages-img">
                        <img src="<?php echo $item['frontpage_advantages_img']; ?>" alt="image">
                        <h4><?php echo $item['frontpage_advantages_title']; ?></h4>
                    </div>
                    <div class="advantages-info">
                        <?php echo $item['frontpage_advantages_desc']; ?>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>
    </div>
</div>