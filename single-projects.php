<?php get_header(); ?>

    <?php while ( have_posts() ): the_post(); ?>


      <div class="project-single-section">
          <div class="container">

              <ul class="breadcrumbs">
                  <li class="breadcrumbs-item"><a href="" class="breadcrumbs-link">Проекты</a></li>
                  <li class="breadcrumbs-item"><a href="" class="breadcrumbs-link">EV-Net</a></li>
<!--                  <li class="breadcrumbs-item"><a href="" class="breadcrumbs-link">EV-Net</a></li>-->
              </ul>
              
              <div class="project-single-block">
                  <div class="project-single-img" style="">
                      <div class="project-single-info">
                          <span><?php echo get_post_meta( get_the_ID(), 'single_project_comand_subtitle', true ); ?></span>
                          <h1><?php the_title(); ?></h1>
                          <p>
                              <?php echo get_post_meta( get_the_ID(), 'single_project_comand_excerpt', true ); ?>
                          </p>
                      </div>
                      <img src="<?php echo get_post_meta( get_the_ID(), 'single_project_image', true ); ?>" alt="image">
                      <!-- /.project-single-info -->
                  </div>
                  <!-- /.project-single-img -->
                  <div class="project-single-text">
                      <?php the_content(); ?>
                  </div>
                  <!-- /.project-single-text -->
              </div>
              <!-- /.project-single-block -->

          </div>
          <!-- /.container -->
      </div>

    <?php endwhile; ?>

<?php get_footer(); ?>